<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 9/20/2017
 * Time: 10:47 AM
 */

namespace App\Student;
use App\Connecton;

class Student extends Connecton {
    private $name;
    private $email;
    private $address;


    public function set($data){
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }

        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }

        if(array_key_exists('address',$data)){
            $this->address=$data['address'];
        }

    }

    public function insertData(){
        $sql="insert into myinfo (name,email,address) values(:name,:email,:address)";
        $stmt=$this->con->prepare($sql);
        $result=$stmt->execute(array(':name'=>$this->name,':email'=>$this->email,':address'=>$this->address));
//        $stmt->bindValue(':name',$this->name);
//        $stmt->bindValue(':email',$this->email);
//        $stmt->bindValue(':address',$this->address);
        if($result){
            session_start();
            $_SESSION['msg']="Data inserted";
            header("location:index.php");
        }
    }

}