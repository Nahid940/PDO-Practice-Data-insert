<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 9/20/2017
 * Time: 10:46 AM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 9/20/2017
 * Time: 11:14 AM
 */
?>

<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 9/20/2017
 * Time: 10:47 AM
 */
session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div class="row">

        <div class="col-md-6">
            <h3>Hello</h3>
            <?php
                if(isset($_SESSION['msg'])){
                    echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                    session_unset();
                }
            ?>
        </div>
    </div>
</div>

</body>
</html>


