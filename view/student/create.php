<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 9/20/2017
 * Time: 11:14 AM
 */
?>

<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 9/20/2017
 * Time: 10:47 AM
 */
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div class="row">

        <div class="col-md-6">
            <h3>Insert Information</h3>
            <form action="store.php" method="post">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="Email">Enter email</label>
                    <input type="email" class="form-control" name="email">
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" name="address" class="form-control">
                </div>

                <div class="gorm-group pull-right">
                    <button type="submit" name="insert" class="btn btn-success">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>

